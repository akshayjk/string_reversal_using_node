const input = process.stdin;

function strRev(string) {
    let str = string;
    let revStr = "";
    for(let i = str.length-2; i>=0; i--) {
        revStr= revStr + str.charAt(i);
    }
    return revStr;
}

input.setEncoding('utf-8');
console.log('Enter you string:');

input.on('data', (data)=>{
    if(toString(data) == 'exit\n'){
        console.log("User input complete, program exit.");
        process.exit();
    } else {
        console.clear();
        let result = strRev(data);
        console.log(result +" is the reversed string " );
        process.exit();
    }
})